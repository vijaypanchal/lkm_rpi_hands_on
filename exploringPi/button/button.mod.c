#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf230cadf, "module_layout" },
	{ 0x6a424025, "param_ops_bool" },
	{ 0x1e30a605, "param_ops_uint" },
	{ 0xfe990052, "gpio_free" },
	{ 0xc1514a3b, "free_irq" },
	{ 0x2c54c931, "gpiod_unexport" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0xc04ebd1, "gpiod_to_irq" },
	{ 0xf720264e, "gpiod_direction_input" },
	{ 0xaef2426d, "gpiod_export" },
	{ 0xe2df5c3d, "gpiod_direction_output_raw" },
	{ 0x47229b5c, "gpio_request" },
	{ 0xfbaf3e66, "kobject_put" },
	{ 0x1731d679, "sysfs_create_group" },
	{ 0xf28178a1, "kobject_create_and_add" },
	{ 0x43cd5771, "kernel_kobj" },
	{ 0x924948ef, "gpiod_get_raw_value" },
	{ 0x6c07ef16, "set_normalized_timespec" },
	{ 0x9ec6ca96, "ktime_get_real_ts64" },
	{ 0x6a8eabbf, "gpiod_set_raw_value" },
	{ 0xdb7305a1, "__stack_chk_fail" },
	{ 0x7c32d0f0, "printk" },
	{ 0xb748c86d, "gpiod_set_debounce" },
	{ 0xe851e37e, "gpio_to_desc" },
	{ 0x8f678b07, "__stack_chk_guard" },
	{ 0x20c55ae0, "sscanf" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x91715312, "sprintf" },
	{ 0xb1ad28e0, "__gnu_mcount_nc" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "480E4925E9EE5DDBFB8398F");
