/* regDev.c
 * */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

dev_t device = MKDEV(235,0);

static int __init init_call(void){
    register_chrdev_region(device,1,"regDevStatic");
    printk(KERN_INFO "Major = %d Minor = %d \n",MAJOR(device), MINOR(device));
    printk(KERN_INFO "Kernel Module Inserted Successfully...\n");
    return 0;
}

static void __exit exit_call(void){
    unregister_chrdev_region(device,1);
    printk(KERN_INFO "Kernel Module Removed Successfully...\n");
    return;
}

module_init(init_call);
module_exit(exit_call);

