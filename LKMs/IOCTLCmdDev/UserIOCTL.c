#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define WR_VER _IOW('w','w',int32_t*)
#define RD_VER _IOR('r','r',int32_t*)


int8_t write_buf[1024];
int8_t read_buf[1024];
int32_t ver = 0;
int main()
{
        int fd;
        char option;
        printf("*********************************\n");

        fd = open("/dev/DevCharIOTCL", O_RDWR);
        if(fd < 0) {
                printf("Cannot open device file...\n");
                return 0;
        }

        while(1) {
                printf("****Please Enter the Option******\n");
                printf("        1. Write                \n");
                printf("        2. Read                 \n");
                printf("        3. Write VER            \n");
                printf("        4. Read VER             \n");
                printf("        5. Exit                 \n");
                printf("*********************************\n");
                scanf(" %c", &option);
                printf("Your Option = %c\n", option);
                
                switch(option) {
                        case '1':
                                printf("Enter the string to write into driver :");
                                scanf("  %[^\t\n]s", write_buf);
                                printf("Data Writing ...");
                                write(fd, write_buf, strlen(write_buf)+1);
                                printf("Done!\n");
                                break;
                        case '2':
                                printf("Data Reading ...");
                                read(fd, read_buf, 1024);
                                printf("Done!\n\n");
                                printf("Data = %s\n\n", read_buf);
                                break;
                        case '3' : 
                                printf("Enter the Version to send\n");
                                scanf("%d",&ver);
                                printf("Writing..\n");
                                ioctl(fd,WR_VER,(int32_t*)&ver);
                                
                                break;
                        case '4' : 
                                printf("Reading Version from Driver\n");
                                ioctl(fd,RD_VER,(int32_t *)&ver);
                                printf("Version is %d\n", ver);
                                break;
                        case '5':
                                close(fd);
                                exit(1);
                                break;
                        default:
                                printf("Enter Valid option = %c\n",option);
                                break;
                }
        }
        close(fd);
}
