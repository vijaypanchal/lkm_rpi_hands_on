/*
 * Dynamic Register Char Device Driver..
 * */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>

dev_t  device = 0;

static int __init entry_func(void){
    
    /*Dynamic Allocation..*/
    if( alloc_chrdev_region(&device,0,1,"devCharDynamic") < 0) {
        printk(KERN_INFO "Cannot allocate Major number and Minor Number \n");
        return -1;
    }
    printk(KERN_INFO "Major = %d Minor = %d \n",MAJOR(device), MINOR(device));
    printk(KERN_INFO "Kernel Module Inserted Successfully...\n");
    return 0;
}


static void __exit exit_func(void){
    unregister_chrdev_region(device, 1);
	printk(KERN_INFO "Kernel Module Removed Successfully...\n");
}

module_init(entry_func);
module_exit(exit_func);

MODULE_LICENSE("GPL");
