/* Auto creating device
 * */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>


dev_t device = 0;
static struct class *device_class;


static int __init entry_call(void){
    /*Allocation of Major Number*/
    
    if( alloc_chrdev_region(&device,0,1,"newCharDev") < 0){
        printk(KERN_ALERT "Error in Dynamic Allocation of Major Number \n");
        return -1;
    }
    printk(KERN_INFO "Major Number = %d and Minor Number = %d \n",MAJOR(device),MINOR(device));
    
    /*Creating struct class*/
    if((device_class = class_create(THIS_MODULE,"newCharDev")) == NULL){
        printk(KERN_ALERT "Cannot create the struct class \n");
        goto r_class;
    }
    printk(KERN_INFO "struct class is succesfully created..\n");
    
    /*Creating Device */
    if((device_create(device_class,NULL,device,NULL,"newCharDev")) == NULL){
        printk(KERN_ALERT "Cannot create device\n");
        goto r_device;
    }
    return 0;
r_device:
    class_destroy(device_class);
r_class:
    unregister_chrdev_region(device,1);
    return -1;
}


static void __exit remove_call(void){
    device_destroy(device_class,device);
    class_destroy(device_class);
    unregister_chrdev_region(device,1);
    printk("Kernel Module is removed..\n");
    return;
}

module_init(entry_call);
module_exit(remove_call);
MODULE_LICENSE("GPL");

