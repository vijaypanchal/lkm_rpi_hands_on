/*Char Device with cDev Structure*/
/*
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/device.h>
#include <linux/cdev.h>*/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>

dev_t charDevice = 0;

static const char *devName = "devChar";

static struct class *device_class;
static struct cdev dev_cdev;

static int __init char_dev_init(void);
static void __exit char_dev_exit(void);
static int charDev_open(struct inode *inode, struct file *file);
static int charDev_release(struct inode *inode, struct file *file);
static ssize_t dev_read(struct file* fileP, char __user * buf, size_t len, loff_t *off);
static ssize_t dev_write(struct file* fileP, const char * buf, size_t len, loff_t *off);

static struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = dev_read,
    .write = dev_write,
    .open = charDev_open,
    .release = charDev_release
};


static int __init char_dev_init(void){
    /*Allocation Major Number*/
    if(alloc_chrdev_region(&charDevice,0,1,devName) < 0){
        printk(KERN_INFO "Error in major number allocation \n");
        return -1;
    }
    printk(KERN_INFO "Allocation of Major Number completed\n");
    
    /*Creating cdev structure*/
    cdev_init(&dev_cdev,&fops);
    
    /*Adding cdev structure to system*/
    
    if(cdev_add(&dev_cdev,charDevice,1) < 0 ){
        printk(KERN_INFO "Error in adding cDev Structure into the system\n");
        goto r_class;
    }
    printk(KERN_INFO "cDev structure addedd into the system\n");
    
    if((device_class = class_create(THIS_MODULE,devName)) == NULL){
        printk(KERN_INFO "Error in creating device class\n");
        goto r_class;
    }
    printk(KERN_INFO "The Device class is created sucessfully\n");
    
    /*Creating a Device*/
    if( device_create(device_class,NULL,charDevice,NULL,devName) == NULL ){
        printk(KERN_INFO "Error while creating the device\n");
        goto r_device;
    }
    return 0;

r_device:
    class_destroy(device_class);
    cdev_del(&dev_cdev);
r_class:
    unregister_chrdev_region(charDevice,1);
    return -1;    
}
static void __exit char_dev_exit(void){
    device_destroy(device_class,charDevice);
    class_destroy(device_class);
    cdev_del(&dev_cdev);
    unregister_chrdev_region(charDevice,1);
    printk(KERN_INFO"The driver module is removed sucessfully!!\n");
    return;
}

static int charDev_open(struct inode *inode, struct file *file){
    printk(KERN_INFO "Driver Open Function Called...!!!\n");
    return 0;
}
static int charDev_release(struct inode *inode, struct file *file){
    printk(KERN_INFO "Driver Release Function Called...!!!\n");
    return 0;
}

static ssize_t dev_read(struct file* fileP, char __user * buf, size_t len, loff_t *off){
    printk(KERN_INFO "Driver Read Function Called...!!!\n");
    return 0;
}
static ssize_t dev_write(struct file* fileP, const char * buf, size_t len, loff_t *off){
    printk(KERN_INFO "Driver write Function Called...!!!\n");
    return len;
}

module_init(char_dev_init);
module_exit(char_dev_exit);
MODULE_VERSION("1.0");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("VJY");
